## v0.4.1

* add usage info
* fixup unnecessary default target branch during update merge request

## v0.4

* update to user interface change in libsaas_gitlab v0.2 

## v0.3

* libsaas_gitlab usage
* create merge requests
* show infos merge requests
* post and show comments - merge request
* update merge request

